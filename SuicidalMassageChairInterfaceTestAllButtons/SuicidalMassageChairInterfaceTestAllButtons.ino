#define buttonCustomA 0
#define buttonCustomB 1
#define buttonCustomC 2
#define buttonLanguage 3
#define LEDSettings 4
#define buttonSetting 5
#define buttonThumb 6
#define buttonRepeat 7
#define buttonNo 8
#define LEDNo 9
#define LEDYes 10
#define buttonYes 11
#define buttonHorn 14
#define buttonKill 15
#define sliderNumbers 24
#define buttonCross 25
#define volume0 21
#define volume1 20
#define sda0 18
#define scl0 19
#define sda1 23
#define scl1 22

int buttons[] = {buttonKill,
                 buttonCustomA,
                 buttonCustomB,
                 buttonCustomC,
                 buttonLanguage,
                 buttonSetting ,
                 buttonThumb,
                 buttonYes,
                 buttonNo,
                 buttonRepeat ,
                 buttonCross,
                 buttonHorn,

                };
int LEDs[] = {LEDSettings, LEDNo, LEDYes, 13};
int arraySize;


volatile int volumeEncoder = 0;
volatile boolean PastA = 0;
volatile boolean PastB = 0;

void setup() {
  Serial.begin(9600);
  //while (!Serial) {
  //}


  arraySize = sizeof(LEDs) / sizeof(LEDs[0]);
  for (int i = 0; i < arraySize; i++)pinMode(LEDs[i], OUTPUT);
  arraySize = sizeof(buttons) / sizeof(buttons[0]);
  for (int i = 0; i < arraySize; i++)pinMode(buttons[i], INPUT_PULLUP);

  pinMode(sliderNumbers, INPUT);
  pinMode(volume0, INPUT_PULLUP);
  pinMode(volume1, INPUT_PULLUP);

  PastA = (boolean)digitalRead(volume0); //initial value of channel A;
  PastB = (boolean)digitalRead(volume1); //and channel B

  attachInterrupt(volume0, doEncoderA, FALLING);
  attachInterrupt(volume1, doEncoderB, CHANGE);

}

void loop() {
  arraySize = sizeof(buttons) / sizeof(buttons[0]);
  for (int i = 0; i < arraySize; i++) {
    Serial.print(digitalRead(buttons[i]));
    Serial.print('\t');
  }
  Serial.print(sliderConversion(analogRead(sliderNumbers)));
  Serial.print('\t');
  Serial.print(volumeEncoder);
  Serial.println();


  arraySize = sizeof(LEDs) / sizeof(LEDs[0]);
  for (int fadeValue = 0 ; fadeValue <= 255; fadeValue += 5) {
    for (int i = 0; i < arraySize; i++)analogWrite(LEDs[i], fadeValue);
    delay(1);
  }
  for (int fadeValue = 255 ; fadeValue >= 0; fadeValue -= 5) {
    for (int i = 0; i < arraySize; i++)analogWrite(LEDs[i], fadeValue);
    delay(1);
  }


}

int sliderConversion(int input){
  //1 730
  //    695
  //2 660
  //    570
  //3 480
  //    407
  //4 335
  //    293
  //5 250
  if(input>695)return 1;
  else if(input>570)return 2;
  else if(input>407)return 3;
  else if(input>293)return 4;
  else return 5;
}

//encoder stuff. FIX BOUNCING
void doEncoderA()
{
  PastB ? volumeEncoder-- :  volumeEncoder++;
}

void doEncoderB()
{
  PastB = !PastB;
}
