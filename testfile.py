from __future__ import print_function
import json
from apiclient import discovery
from httplib2 import Http
from oauth2client import client
from oauth2client import file
from oauth2client import tools

DOCUMENT_ID = '1U-6NhqG_B7QPatIiQAxfuUGw71c-rylCZ9m6AcEzhIg'
SCOPES = 'https://www.googleapis.com/auth/documents'
DISCOVERY_DOC = ('https://docs.googleapis.com/$discovery/rest?'
                 'version=v1')

def read_paragraph_element(element):
    """Returns the text in the given ParagraphElement.

        Args:
            element: a ParagraphElement from a Google Doc.
    """
    text=''
    for elem in element:
        text_run = elem.get('textRun')
        if  text_run:
            text +=text_run.get('content')
    return text

def read_headings(elements):
    text=''
    for value0 in elements:
        if 'paragraph' in value0:         
            for value1 in value0.get('paragraph'):
                if 'bullet' in value1:
                    nestingLevel= value0.get('paragraph').get('bullet').get('nestingLevel')
                    if nestingLevel is None:
                        heading = read_paragraph_element(value0.get('paragraph').get('elements'))
                        if '#opening' in heading.lower():
                            print("main elements are: "+heading)
    return text

def main():
    # Initialize credentials and instantiate Docs API service
    store = file.Storage('token.json')
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets('credentials.json', SCOPES)
        credentials = tools.run_flow(flow, store)
    http = credentials.authorize(Http())
    docs_service = discovery.build(
        'docs', 'v1', http=http, discoveryServiceUrl=DISCOVERY_DOC)
    doc = docs_service.documents().get(documentId=DOCUMENT_ID).execute().get('body').get('content')#immediately filter off fluff
    with open('chaircontent.json', 'w') as f:
        f.write(json.dumps(doc, indent=2, sort_keys=True))
    #print(doc)
    '''
    #use this later to save the google docs data, and load it locally when we turn of internet access

    with open("chaircontent.json", "r") as read_file:
        doc = json.load(read_file)
    '''
    #doc_content = doc.get('body').get('content')
    #print(read_strucutural_elements(doc_content))
    #change_color()
    #print(doc.get('body').get('content'))
    print(read_headings(doc))

if __name__ == '__main__':
    main()