//Code by Arvid&Marie for Full Body Smart Automatic Manipulator, 2019 CAC Shanghai

//Written for the Arduino Leonardo built in on the LattePanda

//to fix:
//pwm frequency? TO AVOID NOISE -> IS MINIMALLY NOTICABLE IRL

#include <FastLED.h>

FASTLED_USING_NAMESPACE

#include <CmdMessenger.h>
#include <AceRoutine.h>
using namespace ace_routine;

#define pump A5
#define mssgup 8
#define mssgdown 7
#define kneading 10
#define pounding 9
#define feet 11

#define vibration 13
#define bellows 5
#define legs 4
#define arms 3
#define shoulders 2

#define chairup A3
#define chairdown A4

#define topstop A1
#define botstop A2

#define redgreen A0

//unused possibilities of the massagechair board
#define blue 100
#define green 100
#define beige 100
#define grey 100
#define pink 100
#define heating1 100 //to remove
#define heating2 100 //to remove
#define led 100      //to remove

const int outputs[] = {pump, mssgup, mssgdown, kneading, pounding, feet, heating1, heating2, vibration, bellows, legs, arms, shoulders, led, chairup, chairdown, redgreen};
const int outputAmount = sizeof(outputs) / sizeof(outputs[0]);
const int inputs[] = {topstop, botstop, blue, green, beige, grey, pink};
const int inputAmount = sizeof(inputs) / sizeof(inputs[0]);

CmdMessenger cmdMessenger = CmdMessenger(Serial);

#define DATA_PIN    6
#define LED_TYPE    WS2811
#define COLOR_ORDER GRB
#define NUM_LEDS    39
CRGB leds[NUM_LEDS];
#define BRIGHTNESS          128
#define FRAMES_PER_SECOND  120

int ledPos = 0;
int ledBreathMin = 100;
int ledBreathMax = 255;

enum
{
  // Commands
  kAcknowledge, // Command to acknowledge that cmd was received
  kError,       // Command to report error
  kReset,
  kStopAll,
  kSetCommand,
  kSetLeds,
};

int kneadingTiming[] = {0, 100};
int poundingTiming[] = {0, 100};
int updownTiming[] = {1000, 2000};
int feetTiming[] = {0, 100};
int legsTiming[] = {0, 100};
int armsTiming[] = {0, 100};
int shouldersTiming[] = {0, 100};
int bellowsTiming[] = {2000, 5000};

int kneadingSpeed = 255;
int poundingSpeed = 255;
int feetSpeed = 255;


float fmap(float x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

COROUTINE(kneadingLoop)
{
  COROUTINE_LOOP()
  {
    if (kneadingTiming[0])
      digitalWrite(kneading, kneadingSpeed);
    COROUTINE_DELAY(kneadingTiming[0]);
    analogWrite(kneading, LOW);
    COROUTINE_DELAY(kneadingTiming[1]);
    COROUTINE_DELAY(10);//to avoid accidental memory drains
  }
}
COROUTINE(poundingLoop)
{
  COROUTINE_LOOP()
  {
    if (poundingTiming[0])
      digitalWrite(pounding, poundingSpeed);
    COROUTINE_DELAY(poundingTiming[0]);
    analogWrite(pounding, LOW);
    COROUTINE_DELAY(poundingTiming[1]);
    COROUTINE_DELAY(10);//to avoid accidental memory drains
  }
}
int upDownProgram = 0;
int upDownDirection = -1; //-1 = down, 1 = up
COROUTINE(updownLoop)
{ //a bit more complex
  //0=back and forth
  //1=all the way up and down
  //
  /*
    static long upDownTimer;
    COROUTINE_LOOP()
    {

    if (upDownProgram == 0)
    {
      upDownTimer=millis();
      if (upDownDirection == -1)        Serial.println("movingDown");
      else if (upDownDirection == 1)        Serial.println("movingUp");
      while(upDownTimer< millis()+updownTiming[0]){
        COROUTINE_YIELD();
        COROUTINE_DELAY(1);
      }
      Serial.println("stopping");
      COROUTINE_DELAY(updownTiming[1]);
      upDownDirection *= -1;
    }
    else if (upDownProgram == 1)
    {
      COROUTINE_DELAY(updownTiming[0]);
      COROUTINE_DELAY(updownTiming[1]);
    }
  */

  /*
    if(upDownProgram==0){

    }
    else if(upDownProgram==1){
    }
    if(upDownDirection)

    if(digitalRead(topstop)||digitalRead(botstop))upDownDirection=upDownDirection*-1

    Serial.print(digitalRead(topstop));
    Serial.print('/t');
    Serial.println(digitalRead(botstop));
    //if(updownTiming[0])digitalWrite(kneading, HIGH);

    digitalWrite(mssgdown, LOW);
    digitalWrite(mssgup, LOW);*/
}

COROUTINE(feetLoop)
{
  COROUTINE_LOOP()
  {
    if (feetTiming[0])
      analogWrite(feet, feetSpeed);
    COROUTINE_DELAY(feetTiming[0]);
    digitalWrite(feet, LOW);
    COROUTINE_DELAY(feetTiming[1]);
    COROUTINE_DELAY(10);//to avoid accidental memory drains
  }
}
COROUTINE(legsLoop)
{
  COROUTINE_LOOP()
  {
    if (legsTiming[0])
      digitalWrite(legs, HIGH);
    COROUTINE_DELAY(legsTiming[0]);
    digitalWrite(legs, LOW);
    COROUTINE_DELAY(legsTiming[1]);
    COROUTINE_DELAY(10);//to avoid accidental memory drains
  }
}
COROUTINE(armsLoop)
{
  COROUTINE_LOOP()
  {
    if (armsTiming[0])
      digitalWrite(arms, HIGH);
    COROUTINE_DELAY(armsTiming[0]);
    digitalWrite(arms, LOW);
    COROUTINE_DELAY(armsTiming[1]);
    COROUTINE_DELAY(10);//to avoid accidental memory drains
  }
}
COROUTINE(shouldersLoop)
{
  COROUTINE_LOOP()
  {
    if (shouldersTiming[0])
      digitalWrite(shoulders, HIGH);
    COROUTINE_DELAY(shouldersTiming[0]);
    digitalWrite(shoulders, LOW);
    COROUTINE_DELAY(shouldersTiming[1]);
    COROUTINE_DELAY(10);//to avoid accidental memory drains
  }
}
COROUTINE(bellowsLoop)
{
  COROUTINE_LOOP()
  {
    if (bellowsTiming[0])digitalWrite(bellows, HIGH);
    COROUTINE_DELAY(bellowsTiming[0]);
    digitalWrite(bellows, LOW);
    COROUTINE_DELAY(bellowsTiming[1]);
    COROUTINE_DELAY(10);//to avoid accidental memory drains
  }
}

bool moveChairDown = false;
COROUTINE(moveChairDownRoutine)
{
  COROUTINE_LOOP()
  {
    COROUTINE_AWAIT(moveChairDown);
    digitalWrite(redgreen, LOW);//go red
    moveChairDown = false;
    digitalWrite(chairdown, HIGH);
    COROUTINE_DELAY(10000);
    digitalWrite(chairdown, LOW);
    COROUTINE_DELAY(10);//to avoid accidental memory drains
    
  }
}

bool moveChairUp = false;
COROUTINE(moveChairUpRoutine)
{
  COROUTINE_LOOP()
  {
    
    COROUTINE_AWAIT(moveChairUp);
    moveChairUp = false;
    digitalWrite(chairup, HIGH);
    COROUTINE_DELAY(15000);
    digitalWrite(chairup, LOW);
    COROUTINE_DELAY(10);//to avoid accidental memory drains
    digitalWrite(redgreen, HIGH);//go green
    
  }
}

COROUTINE(ledLoop)
{
  COROUTINE_LOOP()
  {
    for(int i = 0; i < NUM_LEDS; i ++) {
      leds[i]=CHSV( 100,100, fmap(sin(ledPos), -1, 1, ledBreathMin, ledBreathMax));
    }
    ledPos+=0.1;
    FastLED.show();
    COROUTINE_DELAY(1000 / FRAMES_PER_SECOND);
  }
}

void setup()
{
  Serial.begin(115200);
  while (!Serial)
    ;
  cmdMessenger.printLfCr();
  attachCommandCallbacks();
  for (int i = 0; i < outputAmount; i++)
    pinMode(outputs[i], OUTPUT);
  for (int i = 0; i < inputAmount; i++)
    pinMode(inputs[i], INPUT);
  CoroutineScheduler::setup();

  FastLED.addLeds<LED_TYPE, DATA_PIN, COLOR_ORDER>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);
  FastLED.setBrightness(BRIGHTNESS);

  while (!digitalRead(topstop) && !digitalRead(botstop)) {
    digitalWrite(mssgup, HIGH);
  }
  digitalWrite(mssgup, LOW);
  Serial.println("reached the end");
  digitalWrite(mssgdown, HIGH);
  delay(7000);
  digitalWrite(mssgdown, LOW);

  digitalWrite(redgreen, HIGH);
}

void loop()
{
  CoroutineScheduler::loop();
  cmdMessenger.feedinSerialData();

  checkPump();
  delay(10);
}

// ------------------  C A L L B A C K S -----------------------

void attachCommandCallbacks()
{
  // Attach callback methods
  cmdMessenger.attach(OnUnknownCommand);
  cmdMessenger.attach(kReset, OnReset);
  cmdMessenger.attach(kStopAll, OnStopAll);
  cmdMessenger.attach(kSetCommand, OnSetCommand);
  cmdMessenger.attach(kSetLeds, OnSetLeds);
}

void OnUnknownCommand()
{
  cmdMessenger.sendCmd(kError, "Command without attached callback");
}

void OnArduinoReady()
{
  cmdMessenger.sendCmd(kAcknowledge, "Arduino ready");
}

void OnReset()
{

  //digitalWrite(redgreen, HIGH);
  cmdMessenger.sendCmd(kAcknowledge, "going to idle state");
  OnStopAll();
  moveChairUp = true;
  bellowsTiming[0] = 700;
  bellowsTiming[1] = 6000;
}

void OnStopAll()
{
  cmdMessenger.sendCmd(kAcknowledge, "stopping all");
  kneadingTiming[0] = 0;
  poundingTiming[0] = 0;
  updownTiming[0] = 0;
  feetTiming[0] = 0;
  legsTiming[0] = 0;
  armsTiming[0] = 0;
  shouldersTiming[0] = 0;
  kneadingSpeed = 255;
  poundingSpeed = 255;
  feetSpeed = 255;
}

void OnSetCommand()
{

  //digitalWrite(redgreen, LOW);
  String mssg = cmdMessenger.readStringArg();
  //int val0 = cmdMessenger.readInt16Arg();
  cmdMessenger.sendCmd(kAcknowledge, mssg);

  if (mssg.equals("stopeverything"))
    OnStopAll();
  else if (mssg.equals("greenlight"))
    //digitalWrite(redgreen, HIGH);
    delay(0);
  else if (mssg.equals("redlight"))
    //digitalWrite(redgreen, LOW);
    delay(0);

  else if (mssg.equals("chairupmotor"))
    moveChairUp = true;
  else if (mssg.equals("chairdownmotor"))
    moveChairDown = true;
  else if (mssg.equals("allbodyairbags"))
  {
    String val = cmdMessenger.readStringArg();
    if (val.equals("low"))
      airbagsRandom(20, 100, 500, 2000, 5000, 0, 80);
    else if (val.equals("medium"))
      airbagsRandom(70, 500, 1500, 1500, 3000, 20, 200);
    else if (val.equals("high"))
      airbagsRandom(100, 500, 2000, 1000, 2500, 150, 255);

  }
  else if (mssg.equals("random"))
  {
    String val = cmdMessenger.readStringArg();
    if (val.equals("low"))
      allRandom(20, 100, 500, 2000, 5000, 0, 80);
    else if (val.equals("medium"))
      allRandom(70, 500, 1500, 1500, 3000, 20, 200);
    else if (val.equals("high"))
      allRandom(100, 500, 2000, 1000, 2500, 150, 255);
  }
  else if (mssg.equals("kneading")) {
    kneadingTiming[0] = 1000; kneadingTiming[1] = 0;
  }
  else if (mssg.equals("pouding"))
  {
    poundingTiming[0] = 1000; poundingTiming[1] = 0;
  }
}

void OnSetLeds()
{
}

void checkPump()
{
  if (
    digitalRead(bellows) ||
    digitalRead(legs) ||
    digitalRead(arms) ||
    digitalRead(shoulders)) {
    digitalWrite(pump, HIGH);
  }
  else {
    digitalWrite(pump, LOW);
  }
}

void allRandom(int onAmount, int minOn, int maxOn, int minOff, int maxOff, int minSpeed, int maxSpeed)
{
  cmdMessenger.sendCmd(kAcknowledge, "randomizing all elements");

  kneadingTiming[0] = randFunct(onAmount, minOff, maxOff);
  poundingTiming[0] = randFunct(onAmount, minOff, maxOff);
  updownTiming[0] = randFunct(onAmount, minOff, maxOff);
  feetTiming[0] = randFunct(onAmount, minOff, maxOff);
  legsTiming[0] = randFunct(onAmount, minOff, maxOff);
  armsTiming[0] = randFunct(onAmount, minOff, maxOff);
  shouldersTiming[0] = randFunct(onAmount, minOff, maxOff);

  kneadingTiming[1] = random(minOff, maxOff);
  poundingTiming[1] = random(minOff, maxOff);
  updownTiming[1] = random(minOff, maxOff);
  feetTiming[1] = random(minOff, maxOff);
  legsTiming[1] = random(minOff, maxOff);
  armsTiming[1] = random(minOff, maxOff);
  shouldersTiming[1] = random(minOff, maxOff);

  kneadingSpeed = random(minSpeed, maxSpeed);
  poundingSpeed = random(minSpeed, maxSpeed);
  feetSpeed = random(minSpeed, maxSpeed);
}
void airbagsRandom(int onAmount, int minOn, int maxOn, int minOff, int maxOff, int minSpeed, int maxSpeed)
{
  cmdMessenger.sendCmd(kAcknowledge, "randomizing all elements");

  legsTiming[0] = randFunct(onAmount, minOff, maxOff);
  armsTiming[0] = randFunct(onAmount, minOff, maxOff);
  shouldersTiming[0] = randFunct(onAmount, minOff, maxOff);

  legsTiming[1] = random(minOff, maxOff);
  armsTiming[1] = random(minOff, maxOff);
  shouldersTiming[1] = random(minOff, maxOff);
}
int randFunct(int onAmount, int minOn, int maxOn)
{
  if (random(100) < onAmount)
    return random(minOn, maxOn);
  else
    return 0;
}
