﻿#!/usr/bin/env python

# conversational system and database for the Suicidal Massage Chair
# By Arvid&Marie

# Based on https://github.com/adamlatchem/toolchest





# What we want

#X Tree editor, easy to use for making conversation
#X Mock interface to simulate the experience
#X Easy to add a new conversation
#X Easy to add the proper default buttons
#X let's you see the entries while gopiping through it (two ways)
#X Interface letting you move through the choices
#X choose a (random) next conversation
#X add a kill handler
#X add a log
#X Button for reading text for Google TTS (MARYttl or other)
#X make installation guide
#X Add in basic content for testing
#X change voice for Watson (make it choosable, such that later Amazon(chinese) could be implemented)

# improve interface implementation
    #X -not needed anymore- generate interface based upon the buttons in buttons category (plus wildcard buttons)
    #X repeat button 
    #X response to multiple buttons (+non-responsive button) (makes 1,2,3,4,5 dial work) # same answer to multiple buttons
    #X duplicate buttons of yes/no (default response, but also it's own response)
    # settings button
    # implement interruption (tag, get back to where you started, or new conversation. special dialogue for multiple interruptions)
    #X per convo custom 3 buttons
#X make reading out loud possibe to append or interrupt (multithreading?)
#X tags to move from end of conversation to the start of a new one (and add in log how it moves)
#X implement time up code ( go to ending on next button press)
#X implement timer (+timer settings)
# implement massage visual reference and commands
# no response timer and content -> resetting itself
# make interruption logic
# make repeat button have some extra dialogue (and that after making it repeat a few times it goes to a specific repeat dialogues

# the thing will start from the 'start' tag
# make 'other response' more easy
#X remove customize, affermative,Cross button
# make 'name' box larger (for custom buttons especially)
# make that you can only add button/other/endpoint when rightclicking on Chair Voice
# remove ssml from log
# make it that it is possible to move to a next block without waiting for a buttonpress

# make the starting moment (looping

# make looping content

# add in more draft content for marie and michelle to understand

# implement basic massage programs

# implement arduino code (when in Shanghai)
# make buttons sounds (first with pygame)

# add chinese auto translation (add aplologies for it in the start)

# track the blocks where it has already been (deja vu or random other)

# add volume functionality
# implement midi to trigger sound files for endings and other sound effects (or try having a max/pd patch play the incoming voice file)
# create mute/kill audio
# for settings, make sure no content gets repeated (if content exhausted, always to the last content) maybe also with tags that within specific content specific reactions hppen
# copy paste
#X right click  functions on text
#X  SSML
#   Sound effect 
#   massage commands (specific movement, general intesity)
#X right click functions on tree
#X   add response/button
#X make add response dialog so you can select all the buttons

# add voice looping tag or something

# save all requested voiceclips so they can be played instantly

# remove the start category (make it more simple)
# make text more readable with tags in there (balden what is not a tag? or make tags smaller)
# save log (after each session, when erasing?)
# track data (time, buttons pressed, time elapsed ... )
#X add *create button* dialog (with a choice for default buttons)
#X log scroll to bottom when adding
# (Create auto summary for repeat button)
# drag and drop to move
# if no internet, then use windows voice by default

# clean up code


# structrue
# -root
# --A Start
# --B Conversations
# ---Main topics
# ---...
# --C Endings
# ---Not killed
# ---Killed
# --D Others
# ---Repeat
# ---Interrupt
# ---Settings

# within each convo
# -title
# -tags
# --chair speech
# --other
# --buttons

# for endings
# -end
# -tags

#   var talking
#   var currentblock
#   var state (idle,discussion,ending)
# initialize()-> put chair and all settings back to basic, light on green
#   startblock() -> loops, has some slight movements going going to attract attention (kill switch would already go to ending), light on red when exiting
#   discussion(tags) -> set custom buttons, read text, check if endblock (if so, find a new one to go to after text)
#   readText(text, looping, interruptable)
#   onButtonPress() -> if text is still being spoken, interruption. (unless repeat or settings) find button (or other) to go to next block
#       interrupution(tags) interrupt, repeat, settings... go back to where we came from
#   waiting() -> after waiting a certain time, waiting blocks in a certain order
#  ending(tags) -> not interruptable anymore, at the end go back to initialize
#  timer() -> if a certain time has been reached, next buttonpress will go to timeUp ending


import config
import json
import os
import random
import sys
import threading
import time
import tkinter
import tkinter.ttk as ttk
import webbrowser
from builtins import range, str
from os.path import dirname, join
from pathlib import Path
from random import randrange
from tkinter import messagebox
import re

from google.cloud import texttospeech
from google.cloud.texttospeech import enums
from google.oauth2 import service_account
import pygame
from ibm_watson import TextToSpeechV1
from ibm_watson.websocket import SynthesizeCallback

import PyCmdMessenger

import GUIApplication

try:
    import tkSimpleDialog
    import tkFileDialog
except:
    import tkinter.simpledialog as tkSimpleDialog
    import tkinter.filedialog as tkFileDialog




def augment(text, augmentation):
    if len(text):
        return text + ' : ' + augmentation
    return augmentation
    

class Model(object):
    def __init__(self):
        self.filename = "new.json"
        self.object = {}
         
    def load(self, sourcefile):
        if type(sourcefile) == str:
            sourcefile = open(sourcefile, 'r')
        self.filename = sourcefile.name
        print("opening: "+str(sourcefile))
        json_text = sourcefile.read()
        sourcefile.close()
        self.loads(json_text)

    def loads(self, json_text):
        self.object = json.loads(json_text)

    def save(self, filename=None):
        if filename is None:
            filename = self.filename
        with open(filename, 'w') as file:
            json.dump(self.object, file, sort_keys=True, indent=4,
            separators=(',', ': '))
        self.filename = filename


class ViewModel(object):
    talking=False #a variable to keep track if a voice is talking
    timeup=False
    currentBlock=''
    config.maxtime = 5*60# in seconds
    config.timeron = False
    timer = config.maxtime
    lastText =''
    left_off_point=''
    idleState = False
    language="english"
    
    
    
    def __init__(self, view):
        self.filetypes = (('JSON files', '*.json'),
                           ('All files', '*.*'))
        self.item = None
        self.view = view

        cm = self.view.context_menu
        tm = self.view.text_menu

        self.bind_menu(view.menu_file, 'New', command=self.cmd_new)
        self.bind_menu(view.menu_file, 'Open ...', command=self.cmd_open)
        self.bind_menu(view.menu_file, 'Save', command=self.cmd_save)
        self.bind_menu(view.menu_file, 'Save As ...', command=self.cmd_save_as)
        self.bind_menu(view.menu_file, 'Expand all', command=self.expand_all_nodes)
        self.bind_menu(view.menu_file, 'Collapse all', command=self.collapse_all_nodes)
        self.bind_menu(view.menu_file, 'Quit', command=view.cmd_quit)
        self.bind_menu(view.menu_help, 'Documentation', command=self.cmd_documentation)
        self.bind_menu(view.menu, 'Read Out Loud', command=self.cmd_read)
        self.bind_menu(view.menu, 'Add Conversation', command=self.cmd_add_conversation)
        #self.bind_menu(view.menu, 'Add Response', command=self.cmd_add_response)
        self.bind_menu(view.menu, 'Restart', command=self.cmd_restart)        
        self.bind_menu(view.menu, "Timer Settings", command=self.cmd_timer_settings)       
        self.bind_menu(view.menu, 'Test', command=self.cmd_test)
        self.bind_menu(cm, 'Add Button', command=self.cmd_add_button)
        self.bind_menu(cm, 'Add Other Response', command=self.cmd_add_other)        
        self.bind_menu(cm, 'Add Endpoint', command=self.cmd_add_endpoint)
        self.bind_menu(cm, 'Rename', command=self.cmd_rename)
        self.bind_menu(cm, 'Delete', command=self.cmd_delete)

        self.bind_menu(tm, 'Add voice expression', command=self.cmd_add_voice_expression)
        self.bind_menu(tm, 'Add chair command', command=self.cmd_add_chair_command)
        
        self.view.treeview.bind('<<TreeviewSelect>>', self.on_treeview_select)
        self.view.treeview.bind('<Button-2>', self.on_show_menu)
        self.view.treeview.bind('<Button-3>', self.on_show_menu)
        self.view.treeview.bind('<Button-1>', self.on_treeview_button)
        self.view.parent_label.bind('<Button-1>', self.on_hide_menu)
        self.view.parent_name.bind('<Button-1>', self.on_hide_menu)
        self.view.item_text.bind_class('KeyUp', '<Key>', self.on_item_keyup)
        self.view.item_text.bind('<Button-1>', self.on_hide_menu)
        self.view.item_text.bind('<Button-2>', self.on_show_text_menu)
        self.view.item_text.bind('<Button-3>', self.on_show_text_menu)
        self.view.root.bind('<FocusOut>', self.on_hide_menu)
        

        if len(sys.argv) > 1:
            self.model = Model()
            self.model.load(str(sys.argv[1]))
            self.new_tree()
        else:
            self.cmd_new()

        
        self.update_clock()

        base_path = Path(__file__)
        file = (base_path / "../MassageChairVoice-32a8bd821af1.json").resolve()
        credentials = service_account.Credentials.from_service_account_file(file)
        self.client = texttospeech.TextToSpeechClient(credentials=credentials)

        self.service = TextToSpeechV1(
        url='https://gateway-tok.watsonplatform.net/text-to-speech/api',
        iam_apikey='z4j0i4kSYk6ykrXrI_MN1BXfZbzio1IhcaRf-XjFH9Z_')
        



        #try:
        self.interfaceCommands = 
            [["kAcknowledge","s"],
            ["kError","s"],
            ["kLEDState","s"],
            ["kSetScreen","ss"],
            ["kSendInput","s"],
            ["kSendInputValue","ss"]]

        self.physicalInterfaceBoard = PyCmdMessenger.ArduinoBoard("COM7",baud_rate=115200)
        self.physicalInterface = PyCmdMessenger.CmdMessenger(self.physicalInterfaceBoard,self.interfaceCommands)
        #self.physicalInterface.send("kLEDState","0")

        self.chairCommands = 
            [["kAcknowledge","s"],
            ["kError","s"],
            ["kReset",""],
            ["kStopAll","s"],
            ["kSetCommand","sss"],
            ["kSetLeds","ss"]]


        self.massageChairBoard = PyCmdMessenger.ArduinoBoard("COM5",baud_rate=115200)
        self.massageChair = PyCmdMessenger.CmdMessenger(self.massageChairBoard,self.chairCommands)
        self.massageChair.send("kAcknowledge")

        self.buttonMapping = [
            ["buttonKill","Kill"],
            ["buttonCustomA","custom1"],
            ["buttonCustomB","custom2"],
            ["buttonCustomC","custom3"],
            ["buttonLanguage","Language"],
            ["buttonSettings","Settings"],
            ["buttonThumb","ThumbUp"],
            ["buttonYes","Yes"],
            ["buttonNo","No"],
            ["buttonRepeat","Repeat"],
            ["buttonCross","Cross"],
            ["buttonHorn","Horn"],
            ["sliderNumbers","slider"],
            ["volume",'volume'],
        ]
        #except :
        #    print("Serial connection error")

        threading.Thread(target=self.physicalInterfaceHandler).start()
 
        #pygame.init() #was not needed apparently
        pygame.mixer.init()
        #self.cmd_restart()
        self.view.root.after(3000, self.cmd_restart)

    def cmd_test(self):
        print("test button pressed")
        self.add_to_log("~~test button pressed~~")
        #self.initialize()
        #self.next_conversation()
    
    def update_clock(self):
        mins, secs = divmod(self.timer, 60)
        if self.timer == 0:
            self.cmd_time_up()
        self.view.timer.set( '{:02d}:{:02d}'.format(mins, secs))#time.strftime("%H:%M:%S"))
        if config.timeron:
            self.timer -= 1
        self.view.root.after(1000, self.update_clock)

    def cmd_time_up(self):
        print ("time's up! moving to an ending on the next button press")
        self.timeup=True# this will be checked on buttonpress so that it won't interrupt the current speech and avoid an unanswered question
        #potentially it is nice trigger this also with the waiting response
        
    def cmd_timer_settings(self):
        TimerSettingsDialog(self.view.root)
        self.timer=config.maxtime

    def cmd_add_voice_expression(self):
        
        selection=self.view.item_text.tag_ranges(tkinter.SEL)
        dialog = VoiceExpressionDialog(self.view.root)
        if selection:
             self.view.item_text.insert(selection[1],dialog.endTag)        
             self.view.item_text.insert(selection[0],dialog.startTag)
        else:   
            self.view.item_text.insert(tkinter.INSERT,dialog.startTag+dialog.endTag)    
        #self.view.root.wait_window(dialog.master)
        self.on_item_keyup(' ')
      
    def cmd_add_chair_command(self):
        dialog = ChairCommandDialog(self.view.root)
        self.view.item_text.insert(tkinter.INSERT,dialog.startTag+dialog.endTag)    
        #self.view.root.wait_window(dialog.master)
        self.on_item_keyup(' ')


    def cmd_read(self,interruptable=True,looping=False,waiting=False):
        selected = self.selected()
        blockend=False
        for node in self.view.treeview.get_children(self.view.treeview.parent(selected)):  
            if self.view.treeview.item(node)['values'][0] == "End":#if the end is reached
                    print("end of conversation block reached")
                    blockend=True
                    '''
                            for node in self.view.treeview.get_children(selected):  
                                if self.view.treeview.item(node)['values'][0] == "Chair Voice":#check if there is some text to read
                                        self.set_selection(node)
                                        self.cmd_read(waiting=ending)
                                        if ending:                                       
                                            self.set_selection(node)
                                            self.add_to_log('[End Block reached]')
                                            self.next_conversation()
                    '''

        self.talking=True#start from the moment the function is called
        #if append:
        #    while self.talking:#wait for the last text te be finished
        #        pass
        
        self.view.treeview.see(self.selected()) 
        #text = self.view.item_text.get(1.0, tkinter.END)[:-1]
        text =self.view.treeview.item(self.selected())['values'][1]
        
        
        chaircommands = re.findall('\[.*?\]', text)
        counter = 0
        for test in chaircommands:
            #print("text length approx",len(text)/10,"s, commands",len(chaircommands))
            cmd=test.split(',')[0].replace('[','').replace(']','')

            if cmd == "reset":
                #pygame.mixer.music.stop()
                self.massageChair.send("kSetCommand","stopeverything",'','')
                self.massageChair.send("kSetCommand" ,"chairup",'','')
                config.timeron=False
                self.view.root.after(30000, self.cmd_restart)
                return
            
        text=re.sub("[\[].*?[\]]", "", text) # remove the massage chair commands


        cleanedtext = re.sub("[\<\[].*?[\>\]]", "", text) 
        if looping: 
            cleanedtext = "looping: "+ cleanedtext
        self.add_to_log(cleanedtext)      
        #print("starting to read")

        #if not interruptable:
        #    while self.talking:#wait for the last text te be finished
        #        pass

                #time.sleep(0.5)
                #print("still something being read")
        #if blockend:
        #self.reading_function(text,looping)
        
    #else:
        reading = threading.Thread(target=self.reading_function, args=(text,looping,blockend,))#.start()
        reading.start()
        for test in chaircommands:
            #print("text length approx",len(text)/10,"s, commands",len(chaircommands))
            cmd=test.split(',')[0].replace('[','').replace(']','')

            arg=''
            if len(test.split(',')) > 1:
                 arg=test.split(',')[1].replace('[','').replace(']','')
            #if test.split(',')[1]:arg=test.split(',')[1]
            timedelay = int(counter/len(chaircommands)*len(text)/10+1)#int(counter/len(chaircommands)*len(text)/10+1)
            print("these chair commands are used:",cmd,arg," after ",timedelay,"s")
            #self.view.root.after(timedelay*1000,self.massageChair.send("kSetCommand",cmd,arg,""))
            threading.Thread(target=self.delayedSend, args=(timedelay,cmd,arg,)).start()
            counter+=1 

        
        #ideal situation
        # read text
        # if button interrupts, its possible to check if it is talking
        # if not interruptable then it simply doesn't respond to button press
        # if it is interruptable then it closes and starts a new thing being spoken
        # if it is appendable , then wait until one finishes and a next one starts
    def delayedSend(self,timedelay,cmd,arg):
        time.sleep(timedelay)
        if not self.idleState :
            self.massageChair.send("kSetCommand",cmd,arg,"")

    def reading_function(self,text,looping=False,blockEnd=False):
        pygame.mixer.music.stop()
        pygame.mixer.music.load(join(dirname(__file__), "dummy.mp3"))#unload the mixer
        text=''#temp disabling the voice
        #print("reading: "+ text)
        #make this true to make the Watson voice work
        if text is not '': 
            try:
                if text != self.lastText:

                    if self.language == "english":
                        print("~requesting watson voice~")
                        
                        with open(join(dirname(__file__), 'output.mp3'),'wb') as audio_file:
                            response = self.service.synthesize(
                                '<speak version="1.0">'+text+'</speak>', accept='audio/mp3',
                                voice="en-US_AllisonVoice").get_result()
                            audio_file.write(response.content)
                        print("~voice file received~")
                    elif self.language == "chinese":
                        print("~requesting google voice~")
                        #text="普通话 (中国大陆)"
                        synthesis_input =  texttospeech.types.SynthesisInput(ssml=text)
                        #voices=['A','B','C','D','E','F']
                        voice = texttospeech.types.VoiceSelectionParams(
                            #name="en-US-Wavenet-"+random.choice(voices),
                            language_code='zh',
                            ssml_gender=texttospeech.enums.SsmlVoiceGender.NEUTRAL)
                        audio_config = texttospeech.types.AudioConfig(
                            audio_encoding=texttospeech.enums.AudioEncoding.MP3)
                        response = self.client.synthesize_speech(synthesis_input, voice, audio_config)
                        dir = os.path.dirname(os.path.realpath(__import__("__main__").__file__))
                        file = os.path.join(dir, 'output.mp3')
                        with open(file, 'wb') as out:
                            out.write(response.audio_content)
                else: print("same text as before!")
                #try:
                pygame.mixer.music.load(join(dirname(__file__), "output.mp3"))
                if looping: looping=-1
                pygame.mixer.music.play(loops=looping)
                print("~talking audio~")
                #except:
                #     print("There was an error with the audio loading")
                     #make a fix so that if there is an error here, always the last thing clicks plays (however this only happens when one buttons mashes, so it should have a different response anyways
                while pygame.mixer.music.get_busy():
                    ''
                    #time.sleep(0.5)
                    #print("audio talking")
                print("~audio finished talking~")
            except:
                print("error with the text input")
        self.talking=False
        
        self.lastText = text
        
        if blockEnd: 
            print("the left off point is now",self.left_off_point)
            '''
            if self.left_off_point is not '':
      
                print("returning to left off point",self.left_off_point)
                self.set_selection(self.left_off_point)
                self.view.treeview.see(self.left_off_point) 
                self.left_off_point=''
                return
            '''
            if self.language=="chinese":
                self.next_conversation(cat="B Discussions Chinese")
            else:
                self.next_conversation(cat="B Discussions")

        '''
        elif False: #make this true to make the google voice work
            dir = os.path.dirname(os.path.realpath(__import__("__main__").__file__))
            file = os.path.join(dir, 'dummy.mp3')
            pygame.mixer.music.load(file)
            

            synthesis_input =  texttospeech.types.SynthesisInput(ssml=text)
            voices=['A','B','C','D','E','F']
            voice = texttospeech.types.VoiceSelectionParams(
                name="en-US-Wavenet-"+random.choice(voices),
                language_code='en-US',
                ssml_gender=texttospeech.enums.SsmlVoiceGender.NEUTRAL)
            audio_config = texttospeech.types.AudioConfig(
                audio_encoding=texttospeech.enums.AudioEncoding.MP3)
            response = self.client.synthesize_speech(synthesis_input, voice, audio_config)
            dir = os.path.dirname(os.path.realpath(__import__("__main__").__file__))
            file = os.path.join(dir, 'output.mp3')
            with open(file, 'wb') as out:
                out.write(response.audio_content)
                print('Audio content written to file "output.mp3"')
        
            pygame.mixer.music.load(file)
            pygame.mixer.music.play()
        '''

    def add_to_log(self, text):
        self.view.item_log.insert(tkinter.END,"~"+time.strftime("%H:%M:%S")+" "+text+'\n')
        self.view.item_log.see('end')

    def cmd_add_conversation(self):
        print("adding conversation")
        
        node=self.view.treeview.get_children()[0]
        dialog= NewConversationDialog(self.view.root)
        self.collapse_all_nodes()
        #self.view.root.wait_window(dialog.master) #might be useful to implement if the program should hang in between entries
        #print(dialog.category+ " : "+dialog.story_name)
        for nodes in self.view.treeview.get_children(node):
            if dialog.category == self.view.treeview.item(nodes)['values'][0]:
                #print(nodes)
                #print(dialog.story_name)
                self.set_selection(nodes)
                self.new_node({},dialog.story_name)
                for newnode in self.view.treeview.get_children(nodes):
                    if dialog.story_name == self.view.treeview.item(newnode)['values'][0]:
                        new_story=newnode
                        self.set_selection(newnode)
                        self.new_node(dialog.tags,"Tags")
                        self.set_selection(newnode)
                        self.new_node({},"Start")
                        self.cmd_add_response(dialog.category)
                        self.set_selection(new_story)



    def cmd_check_non_root_selection(self):
        selected = self.selected()
        if self.view.treeview.item(selected)['values'][0] in {'root',"A Start","B Discussions","C Endings","D Interruptions","Tags","End"} :
            print('not making a button to the root categories')
            return
        if self.view.treeview.item(self.view.treeview.parent(selected))['values'][0] in {'root',"A Start","B Discussions","C Endings","D Interruptions","Tags","End"} :
            print('not making a button to the main categories')
            return
        if self.item_type[selected] != dict:
            self.set_selection(self.view.treeview.parent(selected))
            selected = self.selected()
            print( "a string was selected, so moving to its parent")
        return True

    def cmd_add_button(self):
        #FIX: if on other, and it doesn't have a chair speech thing yet, add it
        if self.cmd_check_non_root_selection():
            #print( self.item_type[selected] )

            dialog=NewButtonDialog(self.view.root)
            if dialog.chosenButtons:        
                #newButton=dialog.chosenButtons.split(",")
                self.new_node({},dialog.chosenButtons)
                self.cmd_add_response()
        
    def cmd_add_response(self,category=''):
        
        selected = self.selected()
        self.new_node("~type here what the chair will say~","Chair Voice")
        self.set_selection(selected)
        if category == 'C Endings':
            self.new_node('',"End")
        else:
            self.new_node({},"Other")
        self.set_selection(self.view.treeview.get_children(selected)[0])#setting back to the chair voice
 
    def cmd_add_other(self):
        print("Adding Other")
        if self.cmd_check_non_root_selection():
            if self.item_type[self.selected()] == '':
                print ("string selected, moving to its parent")
                self.set_selection(self.view.treeview.parent(selected()))
            selected = self.selected()
            for val in self.view.treeview.get_children(selected):
                if self.view.treeview.item(val)['values'][0] == 'Other':
                    self.set_selection(val)
                    self.cmd_add_response()
                    return
        self.new_node({},"Other")
        self.cmd_add_response()

    def cmd_add_endpoint(self):        
        print("adding endpoint")
        if self.cmd_check_non_root_selection():
            selected = self.selected()
            
            if self.item_type[selected] == '':
                print ("string selected, moving to its parent")
                selected = self.view.treeview.parent(selected())
                self.set_selection(selected)
            for val in reversed(self.view.treeview.get_children(selected)): #need to reverse as elements change name when deleting
                if self.view.treeview.item(val)['values'][0] != 'Chair Voice':
                    self.set_selection(val)                    
                    print( "deleting "+self.view.treeview.item(val)['values'][0])
                    self.cmd_delete()
            self.set_selection(selected)
            self.new_node("Any,Tags,Go,Here","End")

    def cmd_restart(self):
        self.timer=config.maxtime
        self.currentBlock = ''
        self.idleState=True
        self.massageChair.send("kSetCommand" ,"chairupmotor","","")
        self.massageChair.send("kSetCommand" ,"stopeverything","","")
        

        print('restarted')
        self.collapse_all_nodes()
        self.view.item_log.delete(1.0, tkinter.END)
        self.add_to_log("~started~")


        if self.language=="chinese": 
            category ="A Start Chinese"
        else:
             category ="A Start"
        
        for i in self.view.treeview.get_children():
            self.view.treeview.item(i, open = True)
            for j in self.view.treeview.get_children(i):
                if self.view.treeview.item(j)['values'][0] == category:
                    self.view.treeview.item(j, open = True)
                    chosen_story= random.choice(self.view.treeview.get_children(j))
                    print("restarting at : "+self.view.treeview.item(chosen_story)['values'][0])
                    self.view.treeview.item(chosen_story, open = True)
                    #for node in self.view.treeview.get_children(chosen_story): 
                    #    if self.view.treeview.item(node)['values'][0] == "Start":
                    starting_pont = chosen_story
                    self.view.treeview.item(starting_pont, open = True)
                    for node in self.view.treeview.get_children(starting_pont): 
                        if self.view.treeview.item(node)['values'][0] == "Chair Voice":
                            self.set_selection(node)     
                            self.cmd_read(looping=True)
                            #self.timer=config.maxtime#replace this with the variable
                            return

    def expand_all_nodes(self,start=None):
        if start is None: 
            start=self.view.treeview.get_children()[0]
        #print("expanding all from "+start)
        self.view.treeview.item(start, open = True)
        for node in self.view.treeview.get_children(start):
            if node: self.expand_all_nodes(node)

    def collapse_all_nodes(self,start=None):
        if start is None: 
            start=self.view.treeview.get_children()[0]
        #print("collapsing all")
        self.view.treeview.item(start, open = False)
        for node in self.view.treeview.get_children(start):
            if node: self.collapse_all_nodes(node) 

        #code for finding top element
        #start="I002"
        #while True:
        #    if not self.view.treeview.get_children(start) : break
        #    topelement = self.view.treeview.get_children(start)[-1]
        #    start = topelement
        #print(start)
        
        #print("top element is "+str(self.find_top_element()))
        #self.view.treeview.item(start, open = True)
        #for node in self.view.treeview.get_children(start):
        #    if node: self.expand_all_nodes(node)

    def find_top_element(self,start="I002"):

        while True:
            print("starting fucntion") 
            topelement = self.view.treeview.get_children(start)[-1]
            print("topelement "+str(topelement))            
            print(" start "+str(start))
            if not topelement : 
                return str(start)
            start = topelement

                

        #topelement = self.view.treeview.get_children(start)
        
        #if not topelement : 
        #    print("returning start "+str(start))
        #    return str(start)
        #self.find_top_element(topelement[-1])
        
    def physicalInterfaceHandler(self):
        

            while True:
                try:
                    mssgmsg = self.massageChair.receive()
                    if mssgmsg:
                        print(" received from chair",mssgmsg)
                except:
                    print("something wrong the massage chair input")

                msg = self.physicalInterface.receive()
                if msg:
                    #self.physicalInterface.send("kLEDState","1")

                    for arduinoName, pythonName in self.buttonMapping:
                        if msg[1][0] == arduinoName:
                            if arduinoName is "buttonLanguage":
                                if msg[1][1] is '1':
                                    print("language Chinese")
                                    self.language="chinese"

                                    self.next_conversation(cat="B Discussions Chinese")

                                elif msg[1][1] is '0':
                                    print("language English")
                                    self.language="english"

                                    self.next_conversation()

                            if arduinoName in ["sliderNumbers","volume"]:
                                print(pythonName,msg[1][1], " switched on physical interface")
                            else :
                                print(pythonName, " pressed on physical interface")
                                if pythonName:
                                    self.onbutton_click(pythonName)


    def onbutton_click(self, label):
        if self.talking and not self.idleState and not label == "Kill":
            return
        
        label=str(label)     

        if self.timeup:    
            self.timeup = False        
            self.add_to_log("[Time's up]")
            if self.language=="chinese": self.next_conversation(cat="C Endings Chinese",tags="TimeUp")
            else: self.next_conversation(cat="C Endings",tags="TimeUp")
            return

        #mapping the custom buttons, can be done more elegantly, but it seems to work
        if label == "custom1":
            label=self.view.custom1.cget("text")
        if label == "custom2":
            label=self.view.custom2.cget("text")
        if label == "custom3":
            label=self.view.custom3.cget("text")
        print('button pressed: ', label)
        if label  == '':
            return

        #convert the dupulicate buttons
        if label == "ThumbUp":
            label="Yes"
        if label == "Cross":
            label="No"
     
        #Handle the breaking buttons
        if label == "Kill":
            print("killswitch pressed!")            
            self.add_to_log('['+label+']')
            self.massageChair.send("kSetCommand" ,"chairupmotor","","")
            self.massageChair.send("kReset")
            if self.language=="chinese": self.next_conversation(cat="C Endings Chinese",tags="killswitchpressed")
            else: self.next_conversation(cat="C Endings",tags="killswitchpressed")
            #self.cmd_restart()
            return

        if label == "Repeat":
            print("repeat pressed!")
            self.add_to_log('['+label+']')
            self.interruption('repeat')
            self.cmd_read()
            return

        if label in ["Settings"]:
            print("Settings pressed!")
            self.add_to_log('[Settings]')
            self.interruption('settings')
            return

        if self.talking and not self.idleState:
            print("interrupted!")
            self.add_to_log('~interrupted by user~')
            self.interruption('interruption')

        if self.idleState:
            #put light to red
            self.massageChair.send("kSetCommand","chairdownmotor","","")
            config.timeron = True
            self.idleState=False
            self.talking=False
            print("leaving idle state")
            pass

        selected_item = self.selected()
        if selected_item:#don't proceed if nothing has been selected
            selected_label = self.view.treeview.item(selected_item)['values'][0]
            if selected_label != "Chair Voice": #only proceed if chair voice has been selected
                print("not a proper selection, you should select Chair Voice")
            else :
                chosenbutton=None
                for node in self.view.treeview.get_children(self.view.treeview.parent(selected_item)): 
                    for options in self.view.treeview.item(node)['values'][0].split(','):
                        if options == label:#check if a response is availabe to the pressed button
                            chosenbutton=node
                if chosenbutton==None:#if not proper button was found, got to the 'other' response
                    print("no button for this dialogue")
                    for node in self.view.treeview.get_children(self.view.treeview.parent(selected_item)): 
                        if "Other" in self.view.treeview.item(node)['values'][0]:
                            label+=' = Other]'
                            chosenbutton=node
                        
                try: 
                    self.view.treeview.item(chosenbutton, open = True)
                except:
                    print("crashing interface issue is here")
                self.add_to_log('['+label+']')
       
                for node in self.view.treeview.get_children(chosenbutton):  
                    if self.view.treeview.item(node)['values'][0] == "Chair Voice":#check if there is some text to read
                        self.set_selection(node)
                        self.cmd_read()
                    

                                        #add in something with tags
                             

        #print( self.view.treeview.item(selected_item)['values'][0])
        #if kill, then move to a kill section
        #check the children if there is one with the right label
            #if not, then do the "other" thing
        #otherwise go and read the section with the right label

    def next_conversation(self,cat="B Discussions",tags=''):
        #while self.talking:
        #    pass
        #tags =
        if tags is '':
            selected = self.selected()
            for node in self.view.treeview.get_children(self.view.treeview.parent(selected)):
                if self.view.treeview.item(node)['values'][0] == "End":
                    tags = self.view.treeview.item(node)['values'][1].split(',')#.remove("")#.lower().replace(" ", "")
                    tags = list(filter(None, tags)) 
        #tags.append(input_tag.lower().split(','))


        
        print("going to a new conversation with tags: " + str(tags))
        
        for i in self.view.treeview.get_children():
            self.view.treeview.item(i, open = True)
            for j in self.view.treeview.get_children(i):
                if self.view.treeview.item(j)['values'][0] == cat:
                    self.view.treeview.item(j, open = True)
                    matching_conversations=[]
                    matching_conversation_names=[]
                    for available_conversations in self.view.treeview.get_children(j):
                        for node in self.view.treeview.get_children(available_conversations):
                            if self.view.treeview.item(node)['values'][0] == "Tags":
                                #print(self.view.treeview.item(node)['values'][1].split(','))
                                if any(map(lambda match: match in tags, self.view.treeview.item(node)['values'][1].split(','))):#some code to match two lists
                                    #print(self.view.treeview.item(available_conversations)['values'][0]+ " matches" ) 
                                    matching_conversations.append(available_conversations)
                                    matching_conversation_names.append(self.view.treeview.item(available_conversations)['values'][0])
                                else :
                                    pass
                                    #print("no match")
                            else:
                                pass
                                #print("error, no tags given")
                    
                        #print(self.view.treeview.item(available_conversations)['values'][0]) 
                        #if any(map(lambda match: match in temp, lists))
                    
                    print("found suitable conversations ",str(matching_conversation_names))
                    if matching_conversations:
                        chosen_story= random.choice(matching_conversations)
                        print("chosen next convo "+str(self.view.treeview.item(chosen_story)['values'][0]))
                    else:
                        print("no conversations with a suitable tag")
                        return

                    #self.set_selection(chosen_story)
                    #print("chosen : "+self.view.treeview.item(chosen_story)['values'][0])
                    #print(self.view.treeview.get_children(chosen_story))     
                    self.view.treeview.item(chosen_story, open = True)
                    for node in self.view.treeview.get_children(chosen_story): 
                        if self.view.treeview.item(node)['values'][0] == "Start":
                            starting_pont = node
                            self.view.treeview.item(starting_pont, open = True)
                            for node in self.view.treeview.get_children(starting_pont): 
                                if self.view.treeview.item(node)['values'][0] == "Chair Voice":
                                    self.set_selection(node)
                                    self.cmd_read()
                                    return

    def interruption(self,interruption_type=''):
        self.left_off_point = self.selected()
         # will we need some specifically tagged repeat or interruption dialogue?
        if interruption_type.lower() == 'repeat':
            print("doing some extra repeat convo")
            pass
        elif interruption_type.lower() == 'settings':
            print("going to the settings")
            if self.language=="chinese": self.next_conversation(cat="B Discussions Chinese",tags="settings")
            else: self.next_conversation(tags="settings")
            pass
        elif interruption_type.lower() == 'interruption':
            self.left_off_point = '' #return to normal for implementing interruptions
            print("doing some extra interruption")
            pass
            
    def set_selection(self,node):
        self.view.treeview.selection_set(node)

    def cmd_add_object(self):
        self.new_node({})

    def cmd_rename(self):
        selected = self.selected()
        old_value = self.view.treeview.item(selected, 'values')
      
        name = tkSimpleDialog.askstring('Rename', 'Name:', initialvalue=old_value[0])
        if not name:
            return
        new_text = name + self.view.treeview.item(selected, 'text')[len(old_value[0]):]
        self.view.treeview.item(selected, text=new_text, values=(name, old_value[1]))
        self.view.cmd_dirty()

    def cmd_add_array(self):
        self.new_node([])

    def cmd_move_up(self):
        self.move_selected(-1)

    def cmd_move_down(self):
        self.move_selected(1)

    def cmd_add_string(self):
        self.new_node('')

    def cmd_add_boolean(self):
        self.new_node(True)

    def cmd_add_number(self):
        self.new_node(0.0)

    def cmd_add_null(self):
        self.new_node(None)

    def cmd_delete(self):
        selected = self.selected()
        parent = self.view.treeview.parent(selected)
        if parent == '':
            return
        del self.item_type[selected]
        self.view.treeview.delete(selected)
        self.view.cmd_dirty()

    def cmd_new(self):
        self.model = Model()
        self.view.cmd_dirty()
        self.new_tree()

    def cmd_open(self):
        file = tkFileDialog.askopenfile(filetypes=self.filetypes,
            title='Open JSON File',
            parent=self.view.root)
        if file:
            #print(file)
            self.model = Model()
            self.model.load(file)
            self.view.cmd_clean()
            self.new_tree()

    def cmd_default_open(self):
        dir = os.path.dirname(os.path.realpath(__import__("__main__").__file__))
        file = os.path.join(dir, 'suicidalmassagechairdatabase.json')#change this name in case it should be another name
        #base_path = Path(__file__)
        #file = (base_path / "../suicidalmassagechairdatabase.json").resolve()
        self.model = Model()
        self.model.load(file)
        self.view.cmd_clean()
        self.new_tree()
        self.set_selection(self.view.treeview.get_children())
        
        self.view.add_button()


    def cmd_save(self):
        self.model.loads(self.tree_to_json())
        self.model.save()
        self.view.cmd_clean()
        self.update_title()

    def cmd_save_as(self):
        filename = tkFileDialog.asksaveasfilename(filetypes=self.filetypes,
            title='Save JSON As',
            parent=self.view.root)
        if filename:
            self.model.loads(self.tree_to_json())
            self.model.save(filename)
            self.view.cmd_clean()
            self.update_title()

    def cmd_documentation(self):
        webbrowser.open_new('https://www.arvidandmarie.com/')

    def on_item_keyup(self, event):
        if not self.item is None:
            text = self.view.item_text.get(1.0, tkinter.END)[:-1]
            type = self.item_type[self.item]
            if type == bool:
                cast = lambda x : x.lower().strip() in ['true', '1', 't', 'y', 'yes']
            elif type in (int, float):
                def to_number(text):
                    try:
                        return type(text)
                    except ValueError:
                        return 0
                cast = to_number
            else:
                cast = lambda x : str(x)
            value = str(cast(text))
            values = self.view.treeview.item(self.item, 'values')
            self.view.treeview.item(self.item, text=augment(values[0], value), values=(values[0], value))
            self.view.cmd_dirty()

    def on_treeview_button(self, event):
        #print(str(self.view.treeview.identify_row(event.y))+" "+str(self.view.treeview.identify_column(event.x)))
        #print(self.view.treeview.identify_element(event.x, event.y) )
        #print("clicked on tree")
        #print(self.view.treeview.identify_region(event.x, event.y))
        self.on_hide_menu(event)
        if self.view.treeview.identify_region(event.x, event.y) == "separator":
            return "break"

    def on_treeview_select(self, event):
        
        #print("clicking on tree")
        selected = self.selected()
        if selected == None:
            return
        if selected:
            self.edit(selected)

        if self.cmd_check_non_root_selection:
            #self.set_selection(selected)# to make sure that code runs
                    #print("do something with the buttons")
            #print(self.view.interface) # trying to get the interface array that creates the buttons
            customButtons = []

            #if self.view.treeview.item(selected,'open'):
            #    selected = self.view.treeview.get_children(selected)[0]
            
            for nodeloop in self.view.treeview.get_children(self.view.treeview.parent(selected)): 
                #print ("available thingys "+self.view.treeview.item(nodeloop)['values'][0])
                for label in self.view.treeview.item(nodeloop)['values'][0].split(','):
                    if label not in ['root',"Chair Voice","Other",'Yes','No','Kill','Settings','ThumbUp','Cross','Repeat','1','2','3','4','5','End']:
                        customButtons.append(label)
            customButtons.append('')
            customButtons.append('')#quick and dirty way to make sure we have three empty items at the end of the list
            customButtons.append('')
            #print("custom button = "+str(customButtons))
            #set those buttons
            
            self.view.custom1.config(text=customButtons[0])
            self.view.custom2.config(text=customButtons[1])
            self.view.custom3.config(text=customButtons[2])
            #(potentially fix the parent selection bug)

            self.physicalInterface.send("kSetScreen" ,'0',customButtons[0].encode('ascii','replace'))
            self.physicalInterface.send("kSetScreen" ,'1',customButtons[1].encode('ascii','replace'))
            self.physicalInterface.send("kSetScreen" ,'2',customButtons[2].encode('ascii','replace'))

    def on_show_menu(self, event):
        if self.view.root.focus_get() is None:
            return
        item = self.event_to_item(event)
        
        self.menu_for_item(item)
        
        self.set_selection(item)
        self.view.context_menu.post(event.x_root, event.y_root)

    def on_show_text_menu(self, event):
        #menu = self.view.text_menu
        self.view.text_menu.post(event.x_root, event.y_root)

    def on_hide_menu(self, event):
        self.view.context_menu.unpost()

    def bind_menu(self, menu, entry, **kwargs):
        index = menu.index(entry)
        menu.entryconfig(index, **kwargs)

    def object_to_tree(self, obj, container_id='', key_id=None):
        if container_id == '':
            self.view.treeview.delete(*self.view.treeview.get_children())
            key_id = self.view.treeview.insert(container_id, 'end', text='root')
            self.item_type = {'': 'root', key_id: dict}

        key_item = self.view.treeview.item(key_id)
        key_text = key_item['text']
        if isinstance(obj, dict):
            self.view.treeview.item(key_id, text=augment(key_text, '{ ... }'), values=(key_text, dict))
            self.item_type[key_id] = dict
            for key in sorted(obj):
                inner_key_id = self.view.treeview.insert(key_id, 'end', text=key)
                self.object_to_tree(obj[key], key_item, inner_key_id)
        elif isinstance(obj, list):
            self.view.treeview.item(key_id, text=augment(key_text, '[ ,,, ]'), values=(key_text, list))
            self.item_type[key_id] = list
            for item in obj:
                inner_key_id = self.view.treeview.insert(key_id, 'end', text='')
                self.object_to_tree(item, key_item, inner_key_id)
        else:
            if obj is None:
                value_text = '<null>'
            elif type(obj) in (bool, int, float):
                value_text = str(obj)
            else:
                obj = str(obj)
                value_text = str(obj)
            self.view.treeview.item(key_id, text=augment(key_text, value_text), values=(key_text, value_text))
            self.item_type[key_id] = type(obj)
            if obj is None:
                self.item_type[key_id] = 'null'

    def tree_to_json(self, node_id=''):
        if not node_id:
            node_id = self.view.treeview.get_children()[0] 
        type = self.item_type[node_id]
        tree = self.view.treeview
        if type == dict:
            inner = ''
            for key_id in tree.get_children(node_id):
                if inner:
                    inner += ', '
                value = self.tree_to_json(key_id)
                inner += '"' + tree.item(key_id)['values'][0] + '": ' + value
            return '{' + inner + '}'
        elif type == list:
            inner = ''
            for key_id in tree.get_children(node_id):
                if inner:
                    inner += ', '
                inner += self.tree_to_json(key_id)
            return '[' + inner + ']'
        elif type in (int, float):
            return tree.item(node_id)['values'][1]
        elif type == bool:
            return tree.item(node_id)['values'][1].lower()
        elif type == 'null':
            return 'null'
        else:
            string = str(tree.item(node_id)['values'][1])
            string = string.replace('\\', '\\\\')
            string = string.replace('"', '\\"')
            string = string.replace('\n', '\\n')
            string = string.replace('\t', '\\t')
            return '"' + string + '"'

    def new_tree(self):
        self.object_to_tree(self.model.object)
        self.item = None
        self.set_parent_name('')
        self.view.item_text.delete(1.0, tkinter.END)
        self.update_title()

    def new_node(self, value,key=None):
        container_id = self.selected()
        if self.item_type[container_id] == dict:
            if not key:
                key = str(tkSimpleDialog.askstring('Key name', 'Name:'))
            if not key:
                return
            for child_id in self.view.treeview.get_children(container_id):
                if key == self.view.treeview.item(child_id, 'values')[0]:
                    raise Exception('Key already exists : ' + key)
            key_id = self.view.treeview.insert(container_id, 'end', text=key)
            self.object_to_tree(value, container_id, key_id)
        elif self.item_type[container_id] == list:
            key_id = self.view.treeview.insert(container_id, 'end', text='')
            self.object_to_tree(value, container_id, key_id)
        self.set_selection(key_id)
        self.view.treeview.see(key_id)
        self.view.cmd_dirty()

    def edit(self, item_id):
        type = self.item_type[item_id]
        if type not in (dict, list, 'null'):
            values = self.view.treeview.item(item_id, 'values')
            value_text = self.view.treeview.item(item_id, 'text').replace(values[0] + ' : ', '')

            self.set_parent_name(str(type) + ' ' + values[0])
            self.view.item_text.delete(1.0, tkinter.END)
            self.view.item_text.insert(1.0, value_text)

            self.item = item_id

    def set_parent_name(self, text):
        self.view.parent_name.configure(state=tkinter.NORMAL)
        self.view.parent_name.delete(0, tkinter.END)
        self.view.parent_name.insert(0, text)
        self.view.parent_name.configure(state=tkinter.DISABLED)



    def update_title(self):
        filename = self.model.filename[-50:]
        if filename != self.model.filename:
            filename = '... ' + filename
        self.view.title("Suicidal Massage Chair by Arvid&Marie ~ database: " + filename)

    def menu_for_item(self, item_id):
        type = self.item_type[item_id]
        #print("testing "+str(type))
        context_matrix = {
            #these numbers represent the options in the right-click menu
            'root'  : [0,0,0,0,2,0],
            dict    : [1,1,1,4,2,1],
            list    : [1,1,1,4,2,1],
            str     : [1,1,1,4,2,1],
            int     : [1,1,1,4,2,1],
            float   : [1,1,1,4,2,1],
            bool    : [1,1,1,4,2,1],
            'null'  : [1,1,1,4,2,1],
            
        }
        ''' #old one
            'root'  : [0,0,0,0,0,0,0,0,0,2,0],
            dict    : [1,4,1,3,3,1,1,1,1,2,1],
            list    : [1,4,1,3,3,1,1,1,1,2,1],
            str     : [0,4,0,3,3,0,0,0,0,2,1],
            int     : [0,4,0,3,3,0,0,0,0,2,1],
            float   : [0,4,0,3,3,0,0,0,0,2,1],
            bool    : [0,4,0,3,3,0,0,0,0,2,1],
            'null'  : [0,4,0,3,3,0,0,0,0,2,1],
            '''
        menu = self.view.context_menu
        
        for i in range(len(context_matrix[type])):
            state = context_matrix[type][i]
            parent = self.view.treeview.parent(item_id)
            parent_type = self.item_type[parent]
            
            if state == 0:
                menu.entryconfigure(i, state=tkinter.DISABLED)
            elif state == 1:
                menu.entryconfigure(i, state=tkinter.NORMAL)
            elif state == 3:
                if parent_type == list:
                    menu.entryconfigure(i, state=tkinter.NORMAL)
                else:
                    menu.entryconfigure(i, state=tkinter.DISABLED) 
            elif state == 4:
                if parent_type == dict:
                    menu.entryconfigure(i, state=tkinter.NORMAL)
                else:
                    menu.entryconfigure(i, state=tkinter.DISABLED)
                    

    def move_selected(self, offset):
        selected = self.selected()
        parent = self.view.treeview.parent(selected)
        index = self.view.treeview.index(selected)
        index = index + offset
        self.view.treeview.move(selected, parent, index)
        self.view.cmd_dirty()

    def selected(self):
        selection = self.view.treeview.selection()
        if len(selection) == 1:
            return selection[0]
        return None

    def event_to_item(self, event):
        return self.view.treeview.identify('item', event.x, event.y)



class SuicidalMassageChairSystem(GUIApplication.GUIApplication):
    def __init__(self, root):
        super(SuicidalMassageChairSystem, self).__init__(root, 'SuicidalMassageChairSystem')
        self.create_widgets()
        #self.apply_style(root, 'white')
        
        self.viewmodel = ViewModel(self)
        self.viewmodel.cmd_default_open() #to open the default file
        root.bind('<Control-s>',self.hotkey_save )
        root.protocol("WM_DELETE_WINDOW", self.cmd_quit())#to make the 'want to quit without saving' dialog pop up on closing the window with the x

    def hotkey_save(self, event): 
        self.viewmodel.cmd_save()
     
    def create_menu(self):
        self.menu = tkinter.Menu(self.root)

        self.menu_file = tkinter.Menu(self.menu, tearoff=False)
        self.menu_file.add_command(label='New')
        self.menu_file.add_command(label='Open ...')
        self.menu_file.add_separator()
        self.menu_file.add_command(label='Save')
        self.menu_file.add_command(label='Save As ...')
        self.menu_file.add_separator()
        self.menu_file.add_command(label='Expand all')
        self.menu_file.add_command(label='Collapse all')
        self.menu_file.add_separator()
        self.menu_file.add_command(label='Quit')

        self.menu_help = tkinter.Menu(self.menu, tearoff=False)
        self.menu_help.add_command(label='Version 0.0.1', state=tkinter.DISABLED)
        self.menu_help.add_command(label='Documentation')


        self.root.config(menu=self.menu)
        self.menu.add_cascade(label='File', menu=self.menu_file)
        self.menu.add_cascade(label='Help', menu=self.menu_help)    
        self.menu.add_cascade(label='Read Out Loud', command="cmd_read")         
        self.menu.add_cascade(label='Add Conversation', command="cmd_add_conversation")
        #self.menu.add_cascade(label='Add Response', command="cmd_add_response")
        self.menu.add_cascade(label='Restart', command="cmd_restart")               
        self.menu.add_cascade(label="Timer Settings", command="cmd_timer_settings")
        self.menu.add_cascade(label='Test', command="cmd_test")


    def create_context_menu(self):
        menu = tkinter.Menu(self.root, tearoff=False)
        menu.add_command(label='Add Button')
        menu.add_command(label='Add Other Response')
        menu.add_command(label='Add Endpoint')
        menu.add_command(label='Rename')
        menu.add_separator()
        menu.add_command(label='Delete') 

        self.context_menu = menu

    def create_text_menu(self):
        menu = tkinter.Menu(self.root, tearoff=False)
        menu.add_command(label='Add voice expression')        
        menu.add_command(label='Add chair command')             
        menu.add_command(label='Add sound effect',state=tkinter.DISABLED)
        self.text_menu = menu

    def create_widgets(self):
        self.create_menu()
        self.create_context_menu()
        self.create_text_menu()

        self.pane = tkinter.PanedWindow(self.root, orient=tkinter.HORIZONTAL, sashwidth=4, showhandle=True)
        self.pane.pack(fill=tkinter.BOTH, expand=True)

        self.treeview, self.treeview_scrolled = self.create_scrolled(self.root, ttk.Treeview, True, True)
        self.pane.add(self.treeview_scrolled)
        self.treeview.heading('#0', text='Conversation Tree')

        self.object_frame = tkinter.Frame(self.pane, bg='lightgrey')
        self.object_frame.grid()
        self.pane.add(self.object_frame)

        self.parent_label = tkinter.Label(self.object_frame, text='Item :', foreground='blue', anchor=tkinter.W)
        self.parent_label.grid(column=0, row=0)

        self.parent_name = tkinter.Entry(self.object_frame)
        self.parent_name.grid(column=1, row=0, sticky=tkinter.EW)
        self.parent_name.config(state=tkinter.DISABLED)

        self.timer=tkinter.StringVar()
        self.time_keeper = tkinter.Entry(self.object_frame,textvariable=self.timer)
        self.time_keeper.grid(column=2, row=0, sticky=tkinter.E)
        self.time_keeper.config(state=tkinter.DISABLED)
        #self.timer.set("00:00.00") 

        self.item_text, self.item_text_scrolled = self.create_scrolled(self.object_frame, tkinter.Text, True, True)
        self.extend_bindtags(self.item_text)
        self.item_text.config(wrap=tkinter.WORD)

        self.item_text_scrolled.grid(column=0, row=1, columnspan=3, sticky=tkinter.NSEW)

        self.item_log, self.item_log_scrolled = self.create_scrolled(self.object_frame, tkinter.Text, True, True)
        self.extend_bindtags(self.item_log)
        self.item_log.config(wrap=tkinter.WORD)
        self.item_log_scrolled.grid(column=0, row=2, columnspan=3, sticky=tkinter.NSEW)
        #self.item_log.config(state="readonly") #doesn't work

        #self.chair_movements = tkinter.Frame(self.object_frame).grid(column=0, row=3, columnspan=3, sticky=tkinter.NSEW)
        
        '''
        ttk.Progressbar(self.object_frame, orient="horizontal", length=300).grid(row=3)
        tkinter.Label(self.object_frame, text="chair thing:").grid(row=3)
        tkinter.Label(self.object_frame, text="chair thing2:").grid(row=4)
        tkinter.Label(self.object_frame, text="chair thin3g:").grid(row=5)
        tkinter.Label(self.object_frame, text="chair thing:").grid(row=3,column=2)
        tkinter.Label(self.object_frame, text="chair thing2:").grid(row=4,column=1)
        tkinter.Label(self.object_frame, text="chair thin3g:").grid(row=5,column=5)
        '''
        #print("grid size: "+str(self.object_frame.grid_size()))
        self.grid_weights(self.object_frame, [0, 1,1], [0, 1,2])

        self.grid_weights(self.root, [1, 1, 1], [1])

        self.object_interface = tkinter.Frame(self.pane)
        self.pane.add(self.object_interface)

      ############testfunctions  

    def add_button(self):
        h = 3
        w = 10
        gridwidth=3

        self.interface = [
            ['Yes',"button"],
            ['No',"button"],
            ['Kill',"button"],
            ['Settings',"button"],
            ['ThumbUp',"button"],
            ['Cross',"button"],         
            ['Repeat',"button"],                     
            ['Horn',"button"],
            ['1-5',"slider"],
             ]

             #there might be a more elegant way to do this
        self.custom1 = tkinter.Button(self.object_interface,height=h,width=w*3,text="custom1",wraplength=200, command=lambda e="custom1": self.viewmodel.onbutton_click(e))
        self.custom1.grid(row=5, column=0,columnspan=3)
        self.custom2 = tkinter.Button(self.object_interface,height=h,width=w*3,text="custom2",wraplength=200, command=lambda e="custom2": self.viewmodel.onbutton_click(e))
        self.custom2.grid(row=6, column=0,columnspan=3)
        self.custom3 = tkinter.Button(self.object_interface,height=h,width=w*3,text="custom3",wraplength=200, command=lambda e="custom3": self.viewmodel.onbutton_click(e))
        self.custom3.grid(row=7, column=0,columnspan=3)

        for row,label in enumerate(self.interface):
            if self.interface[row][1] == "button":
                self.element = tkinter.Button(self.object_interface,height=h,width=w,text=label[0], command=lambda e=label[0]: self.viewmodel.onbutton_click(e))
            elif self.interface[row][1] == "slider":
                #self.element = tkinter.Scale(self.object_interface,width=w, label=label[0],from_=0., to=5,orient="horizontal",showvalue=False,command=lambda e=label[0], f=row: self.viewmodel.onbutton_click(interface[f][0],e))             
                for x in range(1, 6): 
                    self.element = tkinter.Button(self.object_interface,height=h,width=w,text=x, command=lambda e=x: self.viewmodel.onbutton_click(e))   
                    self.element.grid(row=int((row+x)/gridwidth), column=(row+x)%gridwidth)
                return
            if self.interface[row][0] == "Kill":
                self.element.configure(bg="red")
            self.element.grid(row=int(row/gridwidth), column=row%gridwidth)
        
        #can be done more pretty
        
            

    def testing(self):
        print("testing function called")

        

class NewConversationDialog(tkSimpleDialog.Dialog):
    def body(self, master,title="Add Conversation"):
        if title:
            self.title(title)

        tkinter.Label(master, text="Story Name:").grid(row=0)
        self.storyEntry = tkinter.Entry(master)
        self.storyEntry.grid(row=0, column=1)

        tkinter.Label(master, text="Tags:").grid(row=1)
        self.taggies = tkinter.StringVar()
        self.tagEntry = tkinter.Entry(master,text=self.taggies)
        self.taggies.set("Any")
        self.tagEntry.grid(row=1, column=1)

        self.v = tkinter.StringVar()
        self.v.set("B Discussions")  # initializing the choice
        
        self.categories = [
            "A Start",
            "B Discussions",
            "C Endings",
            "D Interruptions"
        ]
     
        for val, self.categories in enumerate(self.categories):
             tkinter.Radiobutton(master, 
                  text=self.categories,
                  variable=self.v, 
                  value=self.categories).grid(row=2+val, column=1,sticky="w")

        return self.storyEntry # initial focus

    def apply(self):
        
        self.story_name = self.storyEntry.get()
        self.tags = self.tagEntry.get()
        self.category = self.v.get()
        return True

class NewButtonDialog(tkSimpleDialog.Dialog):
    chosenButtons = None
    buttons = ['Yes',                        
                        'ThumbUp',
                        'No',                        
                        'Cross',
                        'Settings',
                        'Repeat',
                        '1',
                        '2',
                        '3',
                        '4',
                        '5',
                        ]

    def body(self, master,title="Add Button"):
        if title:
            self.title(title)

        self.customButtons = []
        self.customButtonValues = []
        for val in range(0,3):
            self.customButtonValues.append(tkinter.StringVar())
            tkinter.Label(master, text="= Custom button "+str(val+1)).grid(row=val, column=1)
            self.customButtons.append(tkinter.Entry(master,textvariable=self.customButtonValues[val]).grid(row=val, column=0))
        
        
        
        self.checkButtons = []
        self.checkButtonState = []
        for val, self.buttons in enumerate(self.buttons):
            self.checkButtonState.append(tkinter.BooleanVar())
            self.checkButtons.append(tkinter.Checkbutton(
                master, text=self.buttons,
                variable=self.checkButtonState[val]
                ).grid(row=3+val, column=0,sticky="w"))
        tkinter.Label(master, text="Make sure you have no double buttons assigned!").grid(row=17, column=0) # could make this dynamic
        
        #return self.customButtons[0] # initial focus

    def apply(self):
        buttonsTest = ['Yes',                        
                        'ThumbUp',
                        'No',                        
                        'Cross',
                        'Settings',
                        'Repeat',
                        '1',
                        '2',
                        '3',
                        '4',
                        '5',
                        ]
        #a silly fix for a weird problem (self.buttons=5??? doesn't return its proper values...) 

        allbuttons = ''
        for val, self.customButtons in enumerate(self.customButtons):
            if self.customButtonValues[val].get():
                allbuttons+=str(self.customButtonValues[val].get())+','
        for val, self.checkButtons in enumerate(self.checkButtons):
            if self.checkButtonState[val].get():
                allbuttons+=buttonsTest[val]+','
        
        allbuttons = allbuttons[:-1]#remove the last ','
        self.chosenButtons = allbuttons

class TimerSettingsDialog(tkSimpleDialog.Dialog): 

    """ def __init__(self, master,timerstate, maxtime):
        #self.root = tkinter.Toplevel(master, class_=class_)
        self.root = tkinter.Toplevel(master)
        self.frame = tkinter.Frame(self.root)
        self.frame.pack()

        self.master=master
        self.timer_on_off = tkinter.BooleanVar = timerstate        
        self.timer_now = tkinter.BooleanVar = maxtime """
   
    def body(self, master,title="Timer settings"):
        if title:
            self.title(title)
        # timer on/off
        self.timer_on_off = tkinter.BooleanVar()   
        self.timer_on_off.set(config.timeron)     

        #NOW TO ACTUALLY IMPLEMENT THESE SETTINGS (and check if global works instead of config)

        tkinter.Checkbutton(
                master, text="timer on",
                variable=self.timer_on_off
                ).grid(row=0,sticky="w")
        # time setting minutes/seconds 
        self.total_time =  tkinter.IntVar()
        self.total_time.set(str(config.maxtime/60))
        tkinter.Label(master, text="minutes total timer").grid(row=1, column=1)
        tkinter.Entry(master,textvariable=self.total_time).grid(row=1, column=0)
        

    def apply(self):
        config.timeron= self.timer_on_off.get()
        config.maxtime=  self.total_time.get()/60
        
class VoiceExpressionDialog(tkSimpleDialog.Dialog):
    startTag =''
    endTag=''
    #check here for more info https://cloud.ibm.com/docs/services/text-to-speech?topic=text-to-speech-elements 
    def body(self, master,title="Add voice expression"):
        if title:
            self.title(title)

        self.ssml_options=[
            ["break","x-weak, weak, medium, strong, or x-strong, or a specific time like 1s or 1500ms (silences the selected text)"],
            ["phoneme IPA","phonetic pronunciation, eg təˈmeɪ.ɾoʊ = tomato (google for more info)"],
            ["phoneme SPR","phonetic pronunciation, eg .0tx.1me.0Fo = tomato (google for more info)"],
            ["expressive","[GoodNews], [Apology], or [Uncertainty] (specific ways of talking)"],
            ["pitch","[x-low,low,default,high,x-high],[+20Hz],[-10%],[+12st]"],
            ["rate","[x-slow,slow,default,fast,x-fast],[+30%],[40]words per minute"],
            ["volume","[x-soft,soft,medium,loud,default,x-loud],[0->100],"],
            ["pitch_range","[-100%-> 100%],[ x-narrow, narrow, default, wide, x-wide]"],
            ["glottal_tension","[-100%->100%],[ x-low, low, default, high, x-high]"],
            ["breathiness","[-100%->100%],[ x-low, low, default, high, x-high]"],
            ["timbre","[Sunrise] or [Breeze] (specific voice variations)"],
            ["timbre_extent","[0%]->[100%], amount of timbre applied"],
        ]
        #make a label with a link to the full explanation

        
        def callback(url):
            webbrowser.open_new(url)

        self.voice_expression=[]
        for row, item in enumerate(self.ssml_options):
            
            self.voice_expression.append(tkinter.StringVar())
            tkinter.Label(master, text=item[0]).grid(row=row, column=0,sticky="w")
            tkinter.Entry(master,textvariable=self.voice_expression[row]).grid(row=row, column=1,sticky="e")
            tkinter.Label(master, text=item[1]).grid(row=row, column=2,sticky="w")
        url = tkinter.Label(master, text="Click here for more detailed information",fg="blue",cursor="hand2")
        url.grid(row=len(self.ssml_options)+1, column=2,sticky="w")
        url.bind("<Button-1>", lambda e: callback("https://cloud.ibm.com/docs/services/text-to-speech?topic=text-to-speech-elements"))
        tkinter.Label(master, text="MAKE SURE YOU TYPE THE COMMANDS EXCATLY AS IN THE EXAMPLE, SAME CAPITALS, THERE IS NO ERROR SHOWN IF ITS WRONG",fg="red").grid(row=len(self.ssml_options)+2, column=2,sticky="w")

    def apply(self):
        for item, value in enumerate(self.voice_expression):
            if value.get() not in ['',None]:
                if "break" in self.ssml_options[item][0]:
                    variation ="strength"
                    if "s" in value.get():
                        variation ="time"
                    self.startTag = self.startTag+'<break '+variation+'="'+value.get()+'">'
                    self.endTag = '</break>' +self.endTag

                if "phoneme" in self.ssml_options[item][0].lower():
                    variation ="ipa"
                    if "spr" in self.ssml_options[item][0].lower():
                        variation ="spr"
                    self.startTag=self.startTag+ '<phoneme alphabet="'+variation+'" ph="'+value.get()+'">'
                    self.endTag = '</phoneme>'+self.endTag

                if "expressive" in self.ssml_options[item][0]:
                    self.startTag = self.startTag+'<express-as type="'+value.get()+'">'
                    self.endTag = '</express-as>' +self.endTag

                if self.ssml_options[item][0].lower() in ["pitch","rate","volume"] :
                    if 'prosody' not in self.startTag:
                       self.startTag=self.startTag+ '<prosody>'
                    loc = self.startTag.find("prosody")+7
                    self.startTag= self.startTag[:loc]+' '+self.ssml_options[item][0].lower()+'="'+value.get()+'"'+self.startTag[loc:]
                    if '</prosody>' not in self.endTag:
                        self.endTag = '</prosody>'+self.endTag

                if self.ssml_options[item][0].lower() in ["pitch_range","glottal_tension","breathiness","timbre","timbre_extent"] :
                    if 'voice-transformation' not in self.startTag:
                       self.startTag=self.startTag+ '<voice-transformation type="Custom">'
                    loc = self.startTag.find('Custom"')+7
                    self.startTag= self.startTag[:loc]+' '+self.ssml_options[item][0].lower()+'="'+value.get()+'"'+self.startTag[loc:]
                    if '</voice-transformation>' not in self.endTag:
                        self.endTag = '</voice-transformation>'+self.endTag

class ChairCommandDialog(tkSimpleDialog.Dialog):
    startTag =''
    endTag=''
    
    def body(self, master,title="Add chair command"):
        if title:
            self.title(title)

        self.chair_commands=[
            ["program","[lowerbody],[upperbody],[fullbody],[onebyone]"],
            ["intensity","[0]->[100]"],
            ["pounding",""],
            ["kneading","move,speed,three specific positions?"],
            ["updown",""],
            #["vibration",""],#maybe only with voice
            ["shoulders",""],
            ["arms",""],
            ["legs",""],
            ["bellows",""],
            ["feet",""],
            ["chairposition","up,down,middle"],
            ["ledcolor",""],
            ["ledpattern","blinking,breathing,off,on"],
        ]

        self.chair_command=[]
        for row, item in enumerate(self.chair_commands):
            self.chair_command.append(tkinter.StringVar())
            tkinter.Label(master, text=item[0]).grid(row=row, column=0,sticky="w")
            tkinter.Entry(master,textvariable=self.chair_command[row]).grid(row=row, column=1,sticky="e")
            tkinter.Label(master, text=item[1]).grid(row=row, column=2,sticky="w")
        tkinter.Label(master, text="not yet implemented all of this",fg="red").grid(row=len(self.chair_commands)+2, column=2,sticky="w")

           
    def apply(self):
        for item, value in enumerate(self.chair_command):
            if value.get() not in ['',None]:
                print(value.get())
        

if __name__ == '__main__':
    GUIApplication.main(SuicidalMassageChairSystem)
    
