//Code by Arvid&Marie for Full Body Smart Automatic Manipulator, 2019 CAC Shanghai

//Written for a TeensyLC

#include <Bounce2.h>
#include <CmdMessenger.h>
#include <ss_oled.h>
#include <elapsedMillis.h>

#define buttonCustomA 0
#define buttonCustomB 1
#define buttonCustomC 2
#define buttonLanguage 3
#define LEDSettings 4
#define buttonSettings 5
#define buttonThumb 6
#define buttonRepeat 7
#define buttonNo 11
#define LEDNo 10
#define LEDYes 9
#define buttonYes 8
#define buttonHorn 14
#define buttonKill 15
#define sliderNumbers 24
#define buttonCross 25
#define volume0 21
#define volume1 20
#define sda0 18
#define scl0 19
#define sda1 23
#define scl1 22

//########SETTINGS
#define debouncetime 100
//######

CmdMessenger cmdMessenger = CmdMessenger(Serial);

enum
{
  // Commands
  kAcknowledge, // Command to acknowledge that cmd was received
  kError,       // Command to report errors
  kLEDState,    // Command to set the LEDs
  kSetScreen,   // Command to set the content of the screen
  kSendInput,
  kSendInputValue,
};

int buttons[] = {
  buttonKill,
  buttonCustomA,
  buttonCustomB,
  buttonCustomC,
  buttonLanguage,
  buttonSettings,
  buttonThumb,
  buttonYes,
  buttonNo,
  buttonRepeat,
  buttonCross,
  buttonHorn,
};
String buttonsString[] = {"buttonKill",
                          "buttonCustomA",
                          "buttonCustomB",
                          "buttonCustomC",
                          "buttonLanguage",
                          "buttonSettings",
                          "buttonThumb",
                          "buttonYes",
                          "buttonNo",
                          "buttonRepeat",
                          "buttonCross",
                          "buttonHorn"
                         };
int buttonAmount = sizeof(buttons) / sizeof(buttons[0]);
int LEDs[] = {LEDSettings, LEDNo, LEDYes, 13};
int ledAmount = sizeof(buttons) / sizeof(buttons[0]);
Bounce *debouncedButtons = new Bounce[buttonAmount];
int lastSliderValue = 0;
int lastEncoderValue = 0;

volatile int volumeEncoder = 0;
volatile boolean PastA = 0;
volatile boolean PastB = 0;

boolean LEDSOn = true;

elapsedMillis sliderTimer;
bool sliderMoved = false;

#define bufsize 250
char charBuf1[bufsize];
char charBuf2[bufsize];
char charBuf3[bufsize];
char charBuf4[bufsize];
char charBuf5[bufsize];
char szTemp[32];

void setup()
{
  Serial.begin(115200);
  //while (!Serial) {
  //}
  cmdMessenger.printLfCr();
  attachCommandCallbacks();

  for (int i = 0; i < ledAmount; i++)
    pinMode(LEDs[i], OUTPUT);

  for (int i = 0; i < buttonAmount; i++)
  {
    debouncedButtons[i].attach(buttons[i], INPUT_PULLUP);
    debouncedButtons[i].interval(debouncetime);
  }

  pinMode(sliderNumbers, INPUT);
  pinMode(volume0, INPUT_PULLUP);
  pinMode(volume1, INPUT_PULLUP);

  PastA = (boolean)digitalRead(volume0); //initial value of channel A;
  PastB = (boolean)digitalRead(volume1); //and channel B

  attachInterrupt(volume0, doEncoderA, FALLING);
  attachInterrupt(volume1, doEncoderB, CHANGE);

  cmdMessenger.sendCmd(kAcknowledge, "Interface has started!");
  writeToScreen(0, "...." );
  writeToScreen(1, "....."  );
  writeToScreen(2, "..." );

  pinMode(13,OUTPUT);
  digitalWrite(13,HIGH);
}

void loop()
{

  cmdMessenger.feedinSerialData();

  for (int i = 0; i < buttonAmount; i++)
  {
    // Update the Bounce instance :
    debouncedButtons[i].update();
    if (i == 0)
    { //kill is NC
      if (debouncedButtons[i].rose())
      {
        //Serial.print(buttonsString[i]);
        //Serial.println();
        cmdMessenger.sendCmd(kSendInput, buttonsString[i]);
      }
    }
    if (i == 4)
    { //language is switch
      if (debouncedButtons[i].rose() || debouncedButtons[i].fell())
      {
        //Serial.print(buttonsString[i]);
        //Serial.print(" = ");
        //Serial.print(debouncedButtons[i].read());
        //Serial.println();

        cmdMessenger.sendCmdStart(kSendInputValue);
        cmdMessenger.sendCmdArg(buttonsString[i]);
        cmdMessenger.sendCmdArg(debouncedButtons[i].read());
        cmdMessenger.sendCmdEnd();
      }
    }
    else if (debouncedButtons[i].fell())
    {
      //Serial.print(buttonsString[i]);
      //Serial.println();
      cmdMessenger.sendCmd(kSendInput, buttonsString[i]);
    }
  }


  int newSliderValue = sliderConversion(analogRead(sliderNumbers));
  if (lastSliderValue != newSliderValue)
  {
    sliderMoved = true;
    sliderTimer = 0;
    lastSliderValue = newSliderValue;
    //Serial.print("sliderNumbers = ");
    //Serial.print(newSliderValue);
    //Serial.println();
  }
  if (sliderTimer > 500 && sliderMoved) {
    sliderMoved = false;
    //cmdMessenger.sendCmdStart(kSendInputValue);
    //cmdMessenger.sendCmdArg("sliderNumbers");
    cmdMessenger.sendCmdStart(kSendInput);
    cmdMessenger.sendCmdArg(newSliderValue);
    cmdMessenger.sendCmdEnd();
  }



  const int hysterisis = 10;
  int newEncoderValue = volumeEncoder;
  if (newEncoderValue < lastEncoderValue - hysterisis || newEncoderValue > lastEncoderValue + hysterisis)
  {
    //Serial.print("volume = ");
    //Serial.print(newEncoderValue);
    //Serial.println();
    cmdMessenger.sendCmdStart(kSendInputValue);
    cmdMessenger.sendCmdArg("volume");
    cmdMessenger.sendCmdArg(newEncoderValue);
    cmdMessenger.sendCmdEnd();
    lastEncoderValue = newEncoderValue;
  }

  if (LEDSOn)
  {
    if (digitalRead(buttonSettings))
      digitalWrite(LEDSettings, HIGH);
    else
      digitalWrite(LEDSettings, LOW);

    if (digitalRead(buttonYes))
      digitalWrite(LEDYes, HIGH);
    else
      digitalWrite(LEDYes, LOW);

    if (digitalRead(buttonNo))
      digitalWrite(LEDNo, HIGH);
    else
      digitalWrite(LEDNo, LOW);
  }
  else
    for (int i = 0; i < ledAmount; i++)
      digitalWrite(LEDs[i], LOW);
}

int sliderConversion(int input)
{
  //1 730
  //    695
  //2 660
  //    570
  //3 480
  //    407
  //4 335
  //    293
  //5 250
  if (input > 695)
    return 1;
  else if (input > 570)
    return 2;
  else if (input > 407)
    return 3;
  else if (input > 293)
    return 4;
  else
    return 5;
}

//encoder stuff. FIX BOUNCING
void doEncoderA()
{
  PastB ? volumeEncoder-- : volumeEncoder++;
}

void doEncoderB()
{
  PastB = !PastB;
}
// ------------------  C A L L B A C K S -----------------------

void attachCommandCallbacks()
{
  // Attach callback methods
  cmdMessenger.attach(OnUnknownCommand);
  cmdMessenger.attach(kLEDState, OnLEDState);
  cmdMessenger.attach(kSetScreen, OnSetScreen);
}

// Called when a received command has no attached function
void OnUnknownCommand()
{
  cmdMessenger.sendCmd(kError, "Command without attached callback");
}

void OnArduinoReady()
{
  cmdMessenger.sendCmd(kAcknowledge, "Arduino ready");
}

void OnLEDState()
{
  String mssg = cmdMessenger.readStringArg();
  LEDSOn = mssg.toInt();
  //cmdMessenger.sendCmd(kError, LEDSOn);
}

void OnSetScreen() {
  String screenraw = cmdMessenger.readStringArg();
  int screen = screenraw.toInt();
  String text = cmdMessenger.readStringArg();
  if(text)  writeToScreen(screen, text);
  else writeToScreen(screen, " ");

}
const int characterwidth = 14;
//3,screen,text;
void writeToScreen(int screen, String text) {
  Serial.println(text);

  String text1 = text.substring(0 * characterwidth, 1 * characterwidth);
  String text2 = text.substring(1 * characterwidth, 2 * characterwidth);
  String text3 = text.substring(2 * characterwidth, 3 * characterwidth);
  String text4 = text.substring(3 * characterwidth, 4 * characterwidth);  
  String text5 = text.substring(4 * characterwidth, 5 * characterwidth);


  text1.toCharArray(charBuf1, bufsize) ;
  text2.toCharArray(charBuf2, bufsize) ;
  text3.toCharArray(charBuf3, bufsize) ;
  text4.toCharArray(charBuf4, bufsize) ;  
  text5.toCharArray(charBuf5, bufsize) ;
  if (screen == 0)oledInit(OLED_128x64, 0, 0, 17, 16, 400000L); // use standard I2C bus at 400Khz
  if (screen == 1)oledInit(OLED_128x64, 0, 0, 23, 22, 400000L); // use standard I2C bus at 400Khz
  if (screen == 2)oledInit(OLED_128x64, 0, 0, 18, 19, 400000L); // use standard I2C bus at 400Khz
  oledFill(0, 1);
  if (screen > 3) {
    // clear the screens
    return;
  }
  int mod = 0;
  if (screen == 0)mod=-1;
  oledFill(0x0, 1);
  oledWriteString(0, 5, 2+mod, (char *)charBuf1, FONT_NORMAL, 0, 1);
  oledWriteString(0, 5, 3+mod, (char *)charBuf2, FONT_NORMAL, 0, 1);
  oledWriteString(0, 5, 4+mod, (char *)charBuf3, FONT_NORMAL, 0, 1);
  oledWriteString(0, 5, 5+mod, (char *)charBuf4, FONT_NORMAL, 0, 1);
  oledWriteString(0, 5, 6+mod, (char *)charBuf5, FONT_NORMAL, 0, 1);
  Serial.println(text1);
  Serial.println(text2);
  Serial.println(text3);
  Serial.println(text4);
  Serial.println(text5);
  //
  cmdMessenger.sendCmd(kAcknowledge, "screen set");
  //delay(100);
}
