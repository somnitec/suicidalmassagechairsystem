from __future__ import print_function
import json
from apiclient import discovery
from httplib2 import Http
from oauth2client import client
from oauth2client import file
from oauth2client import tools

import random
import sys
import threading
import time

import os
import tkinter as tk
import tkinter.ttk as ttk
import pygubu #to build the interface

DOCUMENT_ID = '1U-6NhqG_B7QPatIiQAxfuUGw71c-rylCZ9m6AcEzhIg'#the link to the docs file!
SCOPES = 'https://www.googleapis.com/auth/documents'
DISCOVERY_DOC = ('https://docs.googleapis.com/$discovery/rest?'
                 'version=v1')

class SuicidalMassagechairSystemv2(pygubu.TkApplication):

    #doc reading code below ~~~~~~~~~~~
    #doc = ''

    def read_paragraph_element(self,element):
        """Returns the text in the given ParagraphElement.

            Args:
                element: a ParagraphElement from a Google Doc.
        """
        text=''
        for elem in element:
            text_run = elem.get('textRun')
            if  text_run:
                text +=text_run.get('content')
        return text

    def read_headings(self,elements):
        text=''
        for value0 in elements:
            if 'paragraph' in value0:         
                for value1 in value0.get('paragraph'):
                    if 'bullet' in value1:
                        nestingLevel= value0.get('paragraph').get('bullet').get('nestingLevel')
                        if nestingLevel is None:
                            heading = self.read_paragraph_element(value0.get('paragraph').get('elements'))
                            if '#opening' in heading.lower():
                                print("main elements are: "+heading)
        return text

    def setupLogic(self):
        # Initialize credentials and instantiate Docs API service
        store = file.Storage('token.json')
        credentials = store.get()
        if not credentials or credentials.invalid:
            flow = client.flow_from_clientsecrets('credentials.json', SCOPES)
            credentials = tools.run_flow(flow, store)
        http = credentials.authorize(Http())
        docs_service = discovery.build(
            'docs', 'v1', http=http, discoveryServiceUrl=DISCOVERY_DOC)
        self.doc = docs_service.documents().get(documentId=DOCUMENT_ID).execute().get('body').get('content')#immediately filter off fluff
        with open('chaircontent.json', 'w') as f:
            f.write(json.dumps(self.doc, indent=2, sort_keys=True))
        print("setupLogic rain")

    #UI code below ~~~~~~~~~~~~~~

    def log(self, text):
        LogText = self.builder.get_object('LogText')
        LogText.insert(tk.END, text+'\n')
        LogText.see('end')
        print("log("+text+")")

    def _create_ui(self):
        s = ttk.Style()
        s.theme_use('clam')
        self.builder = builder = pygubu.Builder()#1: Create a builder        
        builder.add_from_file('suicidalmassagechairinterfacev2.ui')#2: Load an ui file        
        self.gui = builder.get_object('MainFrame', self.master)#3: Create the widget using self.master as parent
        self.set_title('suicidalmassagechairv2')

        builder.connect_callbacks(self)
        RatingSlider = builder.get_object('RatingSlider') #this one needs a custom binding
        RatingSlider.bind("<<ComboboxSelected>>", self.OnRatingSlider)
        self.setupLogic()

    def OnKillButton(self):
        self.log("kill")
        self.read_headings(self.doc)
    def OnCustomButton1(self):      
        RatingSlider = self.builder.get_object('CustomButton1')
        self.log("CustomButton1 "+RatingSlider.cget("text"))  
    def OnCustomButton2(self):      
        RatingSlider = self.builder.get_object('CustomButton2')
        self.log("CustomButton2 "+RatingSlider.cget("text")) 
    def OnCustomButton3(self):      
        RatingSlider = self.builder.get_object('CustomButton3')
        self.log("CustomButton3 "+RatingSlider.cget("text")) 
    def OnLanguageSwitch(self):
        LanguageSwitch = self.builder.get_object('LanguageSwitch')
        self.log("LanguageSwitch "+LanguageSwitch.instate(['selected'])) 
    def OnSettingsButton(self):      
        self.log("Settings")
    def OnThumbButton(self):      
        self.OnYesButton()
    def OnNoButton(self):      
        self.log("No")
    def OnYesButton(self):      
        self.log("Yes")
    def OnRepeatButton(self):      
        self.log("Repeat")
    def OnRatingSlider(self,val):
        RatingSlider = self.builder.get_object('RatingSlider')
        self.log("rating" +RatingSlider.get())
    def OnVolumeEncoder(self):
        VolumeEncoder = self.builder.get_object('VolumeEncoder')
        self.log("volume "+VolumeEncoder.get())
    def OnXButton(self):      
        self.OnNoButton()


if __name__ == '__main__':
    root = tk.Tk()
    app = SuicidalMassagechairSystemv2(root)
    app.run()

